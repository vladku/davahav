import time
import json
import requests

from rich import box
from rich.syntax import Syntax
from rich.console import Console
from rich.rule import Rule
from rich.live import Live
from rich.table import Table
from rich.panel import Panel
from rich.columns import Columns

def get_workers():
    table = Table(row_styles=["", "dim"], box=box.SIMPLE_HEAD)

    table.add_column("Id", style="magenta")
    table.add_column("Name", style="cyan", no_wrap=True)
    table.add_column("Type", style="cyan", no_wrap=True)
    table.add_column("URL", style="magenta")

    # r = requests.get(f"http://localhost:8000/worker/")
    r = requests.get(f"http://localhost:9233/worker/")
    for id, w in r.json().items():
        table.add_row(id, w["name"], w["type"], w["url"])
    return table

def get_tasks():
    table = Table(row_styles=["", "dim"], box=box.SIMPLE_HEAD)

    table.add_column("Id", style="magenta")
    table.add_column("Name", style="cyan", no_wrap=True)
    table.add_column("State", style="magenta")
    table.add_column("Data", style="magenta")

    # r = requests.get(f"http://localhost:8000/task")
    r = requests.get(f"http://localhost:9233/task/")
    for w in r.json():
        table.add_row(w["id"], w["name"], w["state"], str(w["data"]))
    return table

if __name__ == "__main__":
    console = Console()
    with Live(console=console, auto_refresh=False) as live:
        while True:
            live.update(Panel(Columns([Panel(get_workers(), title="Workers"),
                                        Panel(get_tasks(), title="Tasks")]),
                                    title="Monitoring", style="grey46"),
                        refresh=True)
            time.sleep(1)
