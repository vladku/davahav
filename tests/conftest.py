import os
import time
import pytest
import requests
from dataclasses import dataclass

@pytest.fixture(scope="session", autouse=True)
def prepare_davahav_api(request):
    os.popen("cargo run")
    retry_count = 0
    while retry_count < 10:
        try:
            requests.get("http://localhost:9233")
            break
        except requests.exceptions.ConnectionError:
            pass
        time.sleep(.5)
        retry_count += 1
    try:
        yield
    finally:
        c = os.popen("kill -9 $(lsof -ti:9233)")
        c.close()

@pytest.fixture(scope="function")
def prepare_workers(request):
    workers = request.node.get_closest_marker("workers")
    workers = workers.args[0] if workers else [StartWorker(8012, ["1"])]
    try:
        for worker in workers:
            os.popen(f"poetry run python {worker.path} --port {worker.port}"
                f"{''.join([f' --channel {c}' for c in worker.channels])}")
        while True:
            try:
                for worker in workers:
                    requests.get(f"http://localhost:{worker.port}")
                break
            except requests.exceptions.ConnectionError as e:
                pass
            except:
                raise
        yield
    finally:
        for worker in workers:
            c = os.popen(f"kill -9 $(lsof -ti:{worker.port})")
            c.close()

@pytest.fixture(scope="function")
def prepare_go_workers(request):
    try:
        os.popen("cd workers/go && go run . 6789")
        while True:
            try:
                requests.get(f"http://localhost:6789")
                break
            except requests.exceptions.ConnectionError as e:
                pass
            except:
                raise
        yield
    finally:
        c = os.popen(f"kill -9 $(lsof -ti:6789)")
        c.close()

@dataclass
class StartWorker:
    port: int
    channels: list[str]
    path: str = "workers/python/message_queue.py"
