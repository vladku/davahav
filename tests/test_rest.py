import os
import json
import time
import pytest
import requests
from nanoid import generate
from datetime import datetime
from conftest import StartWorker

def create_task(channel, name, data={}) -> str:
    _id = generate(size=10)
    n = f"{datetime.utcnow()}Z"
    r = requests.post(f"http://localhost:9233/task/{channel}", json={
        "id": _id,
        "name": name,
        "state": "NotStarted",
        "data": data,#{"a": {"Str": "test"}},
        "history": [],
        "created_at": n,
        "updated_at": n,
    })
    assert r.status_code == 200
    return _id

def test_create_and_get_by_id():
    _id = create_task("1", "Test")
    result = requests.get(f"http://localhost:9233/task/1/{_id}")
    assert result.status_code == 200
    assert "Test" == result.json()['name']

@pytest.mark.workers([
    StartWorker(8012, ["1"], "workers/python/shard.py")])
def test_shard_worker(prepare_workers):
    #result = requests.get(f"http://localhost:9233/task/1/ShardPy_1").json()
    # assert "ShardPy_One" == result["name"]
    result = requests.get(f"http://localhost:9233/task/").json()
    assert "ShardPy_One" in [r['name'] for r in result]

def test_message_queue_worker(prepare_workers):
    result = requests.get(f"http://localhost:9233/task/").json()
    retry_count = 0
    while "Finished" not in [r['state'] for r in result] and retry_count < 10:
        result = requests.get(f"http://localhost:9233/task/").json()
        time.sleep(.5)
        retry_count += 1
    assert "Finished" in [r['state'] for r in result]

@pytest.mark.workers([
    StartWorker(8012, ["11"]),
    StartWorker(8013, ["22"])])
def test_different_channels_with_message_queue_workers(prepare_workers):
    r = requests.get("http://localhost:9233/worker/")
    assert 200 == r.status_code
    channels = [x["channels"] for _, x in r.json().items()]
    assert ["11"] in channels
    assert ["22"] in channels
    _id1 = create_task("11", "One")
    _id2 = create_task("22", "One")
    t1, t2 = [requests.get(f"http://localhost:9233/task/{c}/{i}").json()
        for c, i in [("11", _id1), ("22", _id2)]]
    retry_count = 0
    while ("Finished" != t1['state'] or "Finished" != t2['state']) and retry_count < 10:
        t1, t2 = [requests.get(f"http://localhost:9233/task/{c}/{i}").json()
            for c, i in [("11", _id1), ("22", _id2)]]
        time.sleep(.5)
        retry_count += 1
    assert "Finished" == t1['state'] and "Finished" == t2['state']

def test_go_worker(prepare_go_workers):
    _id = create_task("go_ch", "One", {"a": {"Int": 123}})
    retry_count = 0
    result = requests.get(f"http://localhost:9233/task/go_ch/{_id}").json()
    while "Finished" == result['state'] and retry_count < 10:
        result = requests.get(f"http://localhost:9233/task/go_ch/{_id}").json()
        time.sleep(.5)
        retry_count += 1
    assert "Finished" == result['state']
    assert 123 == result['data']["a"]["Int"]
