import json
import pytest
from gql import gql, Client
from gql.transport.requests import RequestsHTTPTransport

@pytest.fixture
def gql_client(scope="session"):
    # Select your transport with a defined url endpoint
    transport = RequestsHTTPTransport(url="http://localhost:9233/graphql",
        verify=True,
        retries=3,)

    # Create a GraphQL client using the defined transport
    return Client(transport=transport, fetch_schema_from_transport=True)

# Provide a GraphQL query
query = gql(
    """
    mutation {
        createTasks(channel: "1" tasks: [{
            id: "1",
            name: "One",
            data: [{
                key: "a",
                value: "1"
            },
            {
                key: "b",
                value: "2"
            }]
        }, {
            id: "2",
            name: "Two",
            data: [{
                key: "a",
                value: "123"
            }]
        }]) {
            id,
            name,
            state
        }
    }
"""
)
get_tasks_q = gql(
    """query {
        tasks(filter: {
            #id: "3",
            #name: "[Oo]n"
            #channel: "2"
        }) {
            id
            name
            state
            #createdAt
            #updatedAt
            history {
                at
                state
                by
            }
        }
    }
    """
)

def test_create_task(gql_client):
    gql_client.execute(
        gql("""mutation {
            createTasks(
                channel: "1"
                tasks: [
                    {id: "1", name: "One" data: []}
                    {id: "2", name: "Two" data: []}
                ]
            ) {id}
        }"""))
    result = gql_client.execute(gql("""query { tasks { id name } }"""))
    assert 2 == len(result["tasks"])
    expected = {
        "1": "One",
        "2": "Two"
    }
    for task in result["tasks"]:
        assert expected[task['id']] == task['name']
