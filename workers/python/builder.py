from worker import Task
from fastapi import FastAPI, Depends

def build_fastapi(worker):
    app = FastAPI()
    @app.on_event("startup")
    async def startup_event():
        worker.register()

    @app.get("/ready/", response_model=bool)
    def is_ready():
        return worker.is_ready()

    @app.post("/{channel}/task/", response_model=bool)
    async def create_item(channel: str, task: Task):
        return worker.start(channel, task)

    return app
