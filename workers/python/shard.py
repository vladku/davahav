import time
import uvicorn
import argparse
from datetime import datetime
from typing import List
from pydantic import BaseModel
from worker import Worker, Task, TaskState
from builder import build_fastapi

parser = argparse.ArgumentParser(
    description='Example python shard worker')
parser.add_argument('--port', type=int, help='port for fastapi')
parser.add_argument('--main-url', type=str, help='url to main davahav instance', default="http://localhost:9233/")
parser.add_argument('--channel', nargs='+', type=str,
    help='channel to be processed by worker')
args = parser.parse_args()

w = Worker(
    "ShardPy",
    "Shard",
    args.main_url,
    f"http://localhost:{args.port}",
    ["1"])

app = build_fastapi(w)

@app.get("/task/", response_model=List[Task])
def get_tasks():
    n = f"{datetime.utcnow()}Z"
    return [
        Task("ShardPy_1", "ShardPy_One", TaskState.NotStarted, {}, [], n, n, None)
    ]

if __name__ == "__main__":
    print(f"Starting Shared worker {w.id}...")
    uvicorn.run("shard:app", port=args.port, log_level="info", reload=False)
