import requests
import threading

from enum import Enum
from typing import Optional
from pydantic import BaseModel
from nanoid import generate
from functools import wraps
from dataclasses import dataclass


def task_to_input(task):
    data = []
    for k, v in task.data.items():
        if type(v) == int:
            vv = {"Int": v}
        else:
            vv = {"Str": v}
        data.append({"key":k, "value": vv})
    return {
        "id": task.id,
        "name": task.name,
        "state": task.state,
        "data": data
    }

class TaskState(str, Enum):
    NotStarted = "NotStarted"
    Reserved = "Reserved"
    InProgress = "InProgress"
    Finished = "Finished"
    Error = "Error"

@dataclass
class Task:
    id: str
    name: str
    state: TaskState
    data: dict
    history: list
    created_at: str
    updated_at: str
    next: Optional[object]

class Worker:
    def __init__(self, name, _type, main_url, url, channels, _max=1):
        self.id = generate(size=10)
        self.name = name
        self.type = _type
        self.url = url
        self.main_url = main_url
        self.channels = channels

        self.threads = {}
        self.task_handlers = {}
        self.max = _max

    def do(self, channel, task, _id):
        if task.name not in self.task_handlers:
            return
        self.task_handlers[task.name](channel, task, _id)

    def task_handler(self, name):
        def w(f):
            @wraps(f)
            def w2(channel, task, _id):
                self.update_status(channel, task, _id, TaskState.InProgress)
                try:
                    task = f(task, _id)
                except Exception as e:
                    task.data["Error"] = str(e)
                    self.update_status(channel, task, _id, TaskState.Error)
                else:
                    self.update_status(channel, task, _id, TaskState.Finished)
                finally:
                    del self.threads[task.id]
                    self.ready_do(channel)
            self.task_handlers[name] = w2
            return w2
        return w

    def update_status(self, channel, task, _id, state):
        task.state = state
        r: requests.Response = requests.put(
            f"{self.main_url}task/",
            json={"worker_id": _id, "channel": channel, "task": task_to_input(task)}
        )
        if r.status_code != 200:
            raise Exception(r.text)

    def ready_do(self, channel):
        r: requests.Response = requests.put(
            f"{self.main_url}worker/process/",
            json={"worker_id": self.id, "channel": channel, "by": {"State": TaskState.NotStarted}})
        if r.status_code != 200:
            raise Exception(r.text)

    def start(self, channel, task):
        thread = threading.Thread(target=self.do, args=(channel, task, self.id))
        thread.start()
        self.threads.update({task.id: thread})
        return True

    def is_ready(self):
        return len(self.threads) < self.max + 1

    def register(self):
        ww = {
                "id": self.id,
                "name": self.name,
                "ready": True,
                "type": self.type,
                "url": self.url,
                "channels": self.channels
            }
        print(ww)
        r = requests.post(
            f"{self.main_url}worker/",
            json=ww,
        )
        print(f"Registered with id: {r.text} {r.text == self.id}")
        thread = threading.Thread(target=self.ready_do, args=(self.channels[0],))
        thread.start()
        self.threads.update({"on_register": thread})
