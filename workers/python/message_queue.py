import time
import uvicorn
import argparse
from worker import Worker, Task
from builder import build_fastapi

parser = argparse.ArgumentParser(
    description='Example python message queue worker')
parser.add_argument('--port', type=int, help='port for fastapi')
parser.add_argument('--main-url', type=str,
    help='url to main davahav instance', default="http://localhost:9233/")
parser.add_argument('--channel', nargs='+', type=str,
    help='channel to be processed by worker')
args = parser.parse_args()

w = Worker(
    "MessageQueuePy",
    "MessageQueue",
    args.main_url,
    f"http://localhost:{args.port}",
    args.channel)

@w.task_handler("One")
def one(task, _id):
    #time.sleep(1)
    task.data["One"] = "1"
    # raise Exception("TEst")
    return task

@w.task_handler("Two")
def two(task, _id):
    #time.sleep(2)
    task.data["Two"] = "2"
    return task

app = build_fastapi(w)

if __name__ == "__main__":
    print(f"Starting Message queue worker {w.id}...")
    uvicorn.run("message_queue:app", port=args.port, log_level="info", reload=False)
