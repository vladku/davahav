package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
)

type TaskState int64

// const (
// 	NotStarted TaskState = iota
//     Reserved
//     InProgress
//     Finished
//     Error
// )

// func (s TaskState) String() string {
// 	switch s {
// 	case NotStarted:
// 		return "NotStarted"
// 	case Reserved:
// 		return "Reserved"
// 	case InProgress:
// 		return "InProgress"
// 	case Finished:
// 		return "Finished"
// 	}
// 	return "Error"
// }
type Task struct {
	Id			string 				`json:"id"`
    Name		string				`json:"name"`
    State		string 		  		`json:"state"`
    Data		map[string]interface{} 	`json:"data"`
    History		[]interface{} 		`json:"history"`
	CreatedAt	string 				`json:"created_at"`
    UpdatedAt	string 				`json:"updated_at"`
    Next		*Task 				`json:"next"`
}

func ready(c *gin.Context) {
    c.IndentedJSON(http.StatusOK, true)
}

func startTask(c *gin.Context) {
	channel := c.Param("channel")
	var newTask Task

    if err := c.BindJSON(&newTask); err != nil {
        return
    }
	fmt.Println("%s -> %s", channel, newTask)
	go func () {
		newTask.State = "Finished"
		finnishTask(&newTask)
	}()

    c.IndentedJSON(http.StatusCreated, true)
}

func finnishTask(task *Task) {
	t_data := []map[string]interface{} {}
	for k, v := range task.Data {
		t_data = append(t_data, map[string]interface{} {
			"key": k,
			"value": v,
		})
		log.Printf("Data: %s %s", t_data, strings.ToLower(k))
	}
	t := map[string]interface{} {
		"id": task.Id,
		"name": task.Name,
		"state": task.State,
		"data": t_data,
	}
	payload, err := json.Marshal(map[string]interface{} {
        "worker_id": "Go",
		"channel": "go_ch",
        "task": t,
    })
    if err != nil {
        log.Fatal(err)
    }
	log.Printf("Task: %s", payload)
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodPut, "http://localhost:9233/task/", bytes.NewBuffer(payload))
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	_, err = client.Do(req)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
}

func register() {
	payload, err := json.Marshal(map[string]interface{} {
        "id": "Go",
		"name": "Go_w",
        "ready": true,
		"type": "MessageQueue",
		"url": "http://localhost:6789",
		"channels": []string{"go_ch"},
    })
    if err != nil {
        log.Fatal(err)
    }
	client := &http.Client{}
	log.Printf("Register: %s", payload)
	req, err := http.NewRequest(http.MethodPost, "http://localhost:9233/worker/", bytes.NewBuffer(payload))
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		// handle error
		log.Fatal(err)
	}

    defer resp.Body.Close()

    // 6.
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        log.Fatal(err)
    }
    log.Println(string(body))
}

func main() {
	port := os.Args[1]
    router := gin.Default()
    router.GET("/ready", ready)
    router.POST("/:channel/task", startTask)
	go register()
    router.Run(fmt.Sprintf("localhost:%s", port))
}
