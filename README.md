# davahav

Queue manager tool

Is a try to re-implement something like `Celery` long running task manager.
Manage tasks by Rust server and do not add any requirements for worker technologies.

# Fetures

- Main node with build in-memory storage (other will be added).
- Register workers via REST Api.
  - Suported different scenarios of worker:
    - Replica
    - Shard
    - MessageQueue
    - Streaming
- Push new tasks to node and it will be processed by some worker.
  - Task may contain some data. It can be used as input/output variables.
- Get result of processed task.


# How to build

<del>Rocket (core) requires a 'dev' or 'nightly' version of rustc.
`rustup override set nightly`</del>

`cargo run -- --help`

## Run example
1. `cargo run`.
2. In separate terminal `poetry run python monitor.py`.
3. In separate terminal `poetry run python workers/work.py`.
4. Add any tasks by API.

`cargo run -- -m http://127.0.0.1:9233/worker/ -p 9234 -t shard`


## Note
- To add GRPC https://crates.io/crates/actix-web-tonic

`curl -X POST localhost:9233/task/ -H 'Content-Type: application/json' -d '{"id": "123", "name": "Test", "state": "NotStarted", "data":  {"c": "123"}}'`
kill -9 $(lsof -ti:9233)
poetry run python workers/work.py

## TODO

- <del>Handle worker disconnect/ping</del> Replace ready method to some health check
- ? Make replica type working (now in take only one task!).
    - It should be a full copy of main node.
- <del>Add channels
- Think more about Shard node
- Create nice manage page
- Add possibility to validate worker in channel
- Add grpc version
- Publish python worker lib
- Add failed/retry logic
- Add Redis, Postgres storages
- Add Postpone logic
