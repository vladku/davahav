// use crate::pipeline::{ConnectorType, NodeType, Pipeline, RestConnector};
use crate::connections::{get_shard_tasks, on_notify, ConnectionType};
use crate::storages::*;
use crate::task::{Task, TaskHistory, TaskState};
use crate::workers::*;
use chrono::{DateTime, Utc};
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::thread::JoinHandle;

#[derive(Clone)]
pub struct TaskChannel {
    pub ths: Arc<Mutex<Vec<JoinHandle<()>>>>,
    pub storage: Arc<Mutex<Box<dyn Storage>>>,
    pub workers: Arc<Mutex<HashMap<String, Box<dyn TWorker>>>>,
    pub worker_types: Arc<Mutex<HashMap<WorkerType, fn(Worker) -> Box<dyn TWorker>>>>,
    _health_check_thread: Arc<Mutex<JoinHandle<()>>>,
}
impl std::fmt::Debug for TaskChannel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("LogChannel")
            .field("storage", &self.storage)
            .field("workers", &self.workers)
            .finish()
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub enum ProcessBy {
    Id(String),
    From(DateTime<Utc>),
    State(TaskState),
}

impl TaskChannel {
    fn _new(r#type: WorkerType, master: Option<String>, node: Option<String>) -> Self {
        let mut wt: HashMap<WorkerType, fn(Worker) -> Box<dyn TWorker>> = HashMap::new();
        wt.insert(WorkerType::Shard, ShardWorker::new_t);
        wt.insert(WorkerType::Replica, ReplicaWorker::new_t);
        wt.insert(WorkerType::Streaming, StreamingWorker::new_t);
        wt.insert(WorkerType::MessageQueue, MessageQueueWorker::new_t);
        if let (Some(url), Some(node_url)) = (master, node) {
            let register_thread = std::thread::spawn(move || {
                let client = reqwest::blocking::Client::new();
                let w = Worker {
                    // TODO: Make it async
                    id: node_url.clone(),
                    name: node_url.clone(),
                    r#type: r#type,
                    url: node_url.clone(),
                    channels: vec!["*".to_string()],
                };
                client.post(url).json(&w).send()
            });
            match register_thread.join() {
                Ok(_) => log::info!("Successfully registered!"),
                Err(e) => log::error!("Registration failed: {:?}", e),
            }
        }
        let storage: Arc<Mutex<Box<dyn Storage>>> =
            Arc::new(Mutex::new(Box::new(InMemoryStorage::new())));
        let workers: Arc<Mutex<HashMap<String, Box<dyn TWorker>>>> =
            Arc::new(Mutex::new(HashMap::new()));
        let w = Arc::clone(&workers);
        let s = Arc::clone(&storage);
        let health_check_thread = Arc::new(Mutex::new(std::thread::spawn(move || {
            loop {
                let ten_secs = std::time::Duration::from_secs(10);
                std::thread::sleep(ten_secs);
                // let mut ww = w.lock().expect("Failed to lock workers");
                // let w_ids: Vec<String> = ww
                //     .iter()
                //     .filter(|(_, x)| match x.ready() {
                //         Ok(_) => false,
                //         Err(_) => true,
                //     })
                //     .map(|(_, x)| x.get_id())
                //     .collect();
                // // log::debug!("helth check {:?}", w_ids);
                // for id in w_ids {
                //     log::warn!("Remove worker {} as it is unreached", id);
                //     let worker_del: Option<Box<dyn TWorker>> = ww.remove(&id);
                //     match worker_del {
                //         Some(worker_del) => {
                //             let mut s = s.lock().expect("Failed to lock storage");
                //             for channel in worker_del.get_channels() {
                //                 let tasks = s.get_all(Some(&channel[..]));
                //                 for mut task in tasks {
                //                     if matches!(task.state, TaskState::InProgress)
                //                         && match task.history.last() {
                //                             Some(h) => h.by == id,
                //                             None => false,
                //                         }
                //                     {
                //                         task.state = TaskState::Error;
                //                         s.add(&channel[..], task);
                //                     }
                //                 }
                //             }
                //         }
                //         None => (),
                //     }
                // }
            }
        })));
        Self {
            ths: Arc::new(Mutex::new(vec![])),
            storage,
            workers,
            worker_types: Arc::new(Mutex::new(wt)),
            _health_check_thread: health_check_thread,
        }
    }
    pub fn new_node(r#type: WorkerType, master: String, node: String) -> Self {
        Self::_new(r#type, Some(master), Some(node))
    }
    pub fn new() -> Self {
        Self::_new(WorkerType::Replica, None, None)
    }

    pub async fn notify(&self, channel: &str, task: &Task) {
        let workers = Arc::clone(&self.workers);
        let mut id = "".to_owned();
        {
            let workers = workers.lock();
            match workers {
                Ok(workers) => {
                    let mut massage_send = false;
                    for worker in workers.values() {
                        if worker.get_type() != WorkerType::Shard
                            && (worker.get_type() == WorkerType::Replica
                                || (!massage_send
                                    && worker.get_channels().contains(&channel.to_owned())
                                    && match worker.ready() {
                                        Ok(r) => r,
                                        _ => false,
                                    }))
                        {
                            if worker.get_type() != WorkerType::Replica {
                                massage_send = true;
                            }
                            id = worker.get_id();
                            log::debug!("Notify {} {} with {}", channel, id, task.id.clone());
                        }
                    }
                }
                Err(e) => log::error!("Failed lock workers. {}", e),
            };
        }
        if id == "" {
            log::error!("Failed to find workers for {}.", task.id);
        } else {
            self.process(id, channel, ProcessBy::Id(task.id.clone()))
                .await;
        }
    }
    pub fn push(&self, channel: &str, task: &Task) {
        log::debug!("{} Push task: {:?}", channel, task);
        {
            let storage = Arc::clone(&self.storage);
            let mut storage = storage.lock().unwrap();
            storage.add(channel, task.clone());
        }
    }
    pub async fn push_and_notify(&self, channel: &str, task: &Task) {
        self.push(channel, task);
        self.notify(channel, task).await
    }
    pub async fn add_worker(&self, worker: Box<dyn TWorker>) {
        //} -> Option<Task> {
        let id = worker.get_id();
        let by = match worker.get_type() {
            WorkerType::Streaming => ProcessBy::From(Utc::now()),
            _ => ProcessBy::State(TaskState::NotStarted),
        };
        let ch = worker.get_channels()[0].clone();
        {
            let workers = Arc::clone(&self.workers);
            let mut workers = workers.lock().unwrap();
            workers.insert(id.clone(), worker);
        }
        // std::thread::sleep(std::time::Duration::from_secs(3));
        log::debug!("Worker registered: {:?}", id);
        // self.process(id, &ch[..], by).await
    }

    pub fn remove_worker(&self, id: String) -> Option<Box<dyn TWorker>> {
        let workers = Arc::clone(&self.workers);
        let mut workers = workers.lock().unwrap();
        workers.remove(&id)
    }

    pub async fn process(&self, worker_id: String, channel: &str, by: ProcessBy) -> Option<Task> {
        let task;
        {
            let storage = Arc::clone(&self.storage);
            let storage = storage.lock().expect("Failed to lock storage");
            task = match by {
                ProcessBy::From(from) => storage.get_from(from, Some(channel)),
                ProcessBy::State(state) => storage.get_by(state, Some(channel)),
                ProcessBy::Id(id) => storage.get(channel, &id[..]),
            };
        }
        match task {
            Some(task) => {
                let worker;
                let ch = channel.to_string();
                {
                    let workers = Arc::clone(&self.workers);
                    let workers = workers.lock().expect("Failed to lock workers");
                    worker = match workers.get(&worker_id) {
                        Some(worker) => Some(worker.to_worker()),
                        None => None,
                    }
                }
                // log::warn!("Try send: {:?} to {}", task, worker_id);
                match worker {
                    Some(worker) => {
                        let connection_type = worker.get_connector_type();
                        match on_notify(connection_type, ch, &task, &worker).await {
                            Err(e) => log::error!("Failed to notify worker '{}'. {}", worker_id, e),
                            _ => (),
                        }
                    }
                    None => log::warn!("Failed to find worker: '{}'", worker_id),
                }
                Some(task)
            }
            None => {
                log::debug!("No task to process");
                None
            }
        }
    }

    /// Updates existing task or add a new one.
    pub fn update_task(&self, channel: &str, task: Task, worker_id: String) {
        log::debug!("Update task: {:?}", task);
        let mut original_task;
        {
            let storage = Arc::clone(&self.storage);
            let mut storage = storage.lock().unwrap();
            original_task = match storage.get(channel, &task.id) {
                Some(original_task) => original_task,
                None => task.clone(),
            };
            original_task.history.push(TaskHistory::new(
                task.updated_at.clone(),
                task.state,
                worker_id,
            ));
            original_task.state = task.state;
            original_task.data = task.data;
            storage.add(channel, original_task.clone());
        }
        if matches!(task.state, TaskState::Finished) {
            match original_task.next {
                Some(next_task) => {
                    let mut next_task = next_task.clone();
                    next_task.data = next_task
                        .data
                        .into_iter()
                        .chain(original_task.data)
                        .collect();
                    self.push(channel, &next_task);
                    //self.notify(channel, &next_task).await;
                }
                None => (),
            }
        }
    }

    /// Get all available tasks from storage and shards
    pub async fn get_all(&self, channel: Option<&str>) -> Vec<Task> {
        let mut tasks;
        {
            let storage = Arc::clone(&self.storage);
            let storage = storage.lock().unwrap();
            tasks = storage.get_all(channel);
        }
        tasks.extend(self.get_tasks_from_shards().await);
        tasks
    }

    async fn get_tasks_from_shards(&self) -> Vec<Task> {
        let shard_workers: Vec<(String, ConnectionType)>;
        {
            let w = Arc::clone(&self.workers);
            let w = w.lock().expect("Failed to lock workers");
            shard_workers = w
                .iter()
                .filter(|(_, w)| w.get_type() == WorkerType::Shard)
                .map(|(_, x)| (x.get_url("task/"), x.get_connector_type()))
                .collect();
        }
        let mut tasks = vec![];
        for (url, connector_type) in shard_workers {
            let shard_tasks = match get_shard_tasks(connector_type, url).await {
                Ok(t) => t,
                Err(e) => {
                    log::error!("{}", e);
                    vec![]
                }
            };
            tasks.extend(shard_tasks);
        }
        tasks
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[derive(Clone)]
    pub struct TestWorker {
        id: String,
        r#type: WorkerType,
        ready: bool,
        channels: Vec<String>,
    }
    impl std::fmt::Debug for TestWorker {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("LogChannel")
                .field("id", &self.id)
                .field("type", &self.r#type)
                .field("ready", &self.ready)
                .finish()
        }
    }
    impl TestWorker {
        pub fn new(id: &str, r#type: WorkerType, channels: Vec<String>) -> Self {
            fn f_ready(_: &Task) -> bool {
                true
            }
            TestWorker::new_fn(id, r#type, f_ready, channels)
        }
        fn new_fn(
            id: &str,
            r#type: WorkerType,
            f: fn(&Task) -> bool,
            channels: Vec<String>,
        ) -> Self {
            Self {
                id: id.to_owned(),
                r#type,
                ready: true,
                channels,
            }
        }
    }
    impl TWorker for TestWorker {
        fn get_id(&self) -> String {
            self.id.clone()
        }
        fn get_type(&self) -> WorkerType {
            self.r#type
        }
        fn to_worker(&self) -> Worker {
            Worker::new(
                &self.get_id()[..],
                "Test",
                "test_url",
                WorkerType::Replica,
                self.channels.clone(),
            )
        }
        fn get_channels(&self) -> Vec<String> {
            self.channels.clone()
        }
        fn get_url(&self, path: &str) -> String {
            format!("test_url/{}", path)
        }
        fn ready(&self) -> Result<bool, String> {
            Ok(self.ready)
        }
    }

    #[tokio::test]
    async fn new_manager_have_no_tasks() {
        let channel = TaskChannel::new();
        let t = channel.get_all(None).await;
        assert_eq!(t, vec![]);
    }

    #[tokio::test]
    async fn push_task() {
        let channel = TaskChannel::new();
        let task = Task::new("1", "", TaskState::NotStarted, HashMap::new());
        channel.push("1", &task);
        let t = channel.get_all(None).await;
        assert_eq!(t, vec![task]);
    }
}
