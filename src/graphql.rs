use crate::task::{Task, TaskState, Val};
use crate::task_channel::TaskChannel;
use crate::workers::Worker;

use actix_web::{web, Error, HttpResponse};
use chrono::Utc;
use juniper::{graphql_object, EmptySubscription, FieldResult, RootNode};
use juniper_actix::{graphiql_handler, graphql_handler, playground_handler};
use regex::Regex;
use std::collections::HashMap;
use std::sync::Arc;

#[derive(juniper::GraphQLInputObject)]
pub struct Filter {
    pub id: Option<String>,
    pub name: Option<String>,
    pub channel: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize, juniper::GraphQLInputObject)]
pub struct InVal {
    pub Int: Option<i32>,
    pub Str: Option<String>,
}
impl From<InVal> for Val {
    fn from(val: InVal) -> Self {
        if let Some(v) = val.Int {
            return Val::Int(v);
        } else if let Some(v) = val.Str {
            return Val::Str(v);
        } else {
            Val::None
        }
    }
}
#[derive(Debug, Clone, Serialize, Deserialize, juniper::GraphQLInputObject)]
pub struct InMapEntry {
    pub key: String,
    pub value: InVal,
}

impl juniper::Context for TaskChannel {}
pub struct Query;
#[graphql_object(Context = TaskChannel)]
impl Query {
    #[graphql(description = "List of all workers")]
    fn workers(channel: &TaskChannel) -> FieldResult<Vec<Worker>> {
        Ok(Arc::clone(&channel.workers)
            .lock()
            .unwrap()
            .iter()
            .map(|(_, v)| v.to_worker())
            .collect::<Vec<Worker>>())
    }
    #[graphql(description = "List of all tasks")]
    async fn tasks(filter: Option<Filter>, task_channel: &TaskChannel) -> FieldResult<Vec<Task>> {
        Ok(match filter {
            Some(filter) => {
                let ch = match filter.channel {
                    Some(s) => s,
                    None => "".to_owned(),
                };
                task_channel
                    .get_all(match &ch[..] {
                        "" => None,
                        x => Some(x),
                    })
                    .await
                    .into_iter()
                    .filter(|x| {
                        (match filter.id.clone() {
                            Some(id) => Regex::new(&id).unwrap().is_match(&x.id),
                            None => true,
                        }) && (match filter.name.clone() {
                            Some(name) => Regex::new(&name).unwrap().is_match(&x.name),
                            None => true,
                        })
                    })
                    .map(|x| x.clone())
                    .collect::<Vec<Task>>()
            }
            None => task_channel.get_all(None).await,
        })
    }
}
#[derive(Debug, Clone, Serialize, Deserialize, juniper::GraphQLInputObject)] //, JsonSchema)]
pub struct TaskInput {
    pub id: String,
    pub name: String,
    pub state: Option<TaskState>,
    pub data: Vec<InMapEntry>,
    pub next: Option<Box<TaskInput>>,
}
impl From<TaskInput> for Task {
    fn from(ti: TaskInput) -> Self {
        let now = Utc::now();
        Task {
            id: ti.id,
            name: ti.name,
            next: match ti.next {
                Some(nx) => Some(Box::new(Task::from(*nx))),
                None => None,
            },
            data: to_map(&ti.data),
            history: vec![],
            state: match ti.state {
                Some(s) => s,
                None => TaskState::NotStarted,
            },
            created_at: now,
            updated_at: now,
        }
    }
}

pub fn to_map(in_map: &Vec<InMapEntry>) -> HashMap<String, Val> {
    let mut map = HashMap::new();
    for x in in_map.iter() {
        map.insert(x.key.clone(), Val::from(x.value.clone()));
    }
    map
}

pub struct Mutation;
#[graphql_object(Context = TaskChannel)]
impl Mutation {
    // fn create_workers(channel: &LogChannel) -> FieldResult<Vec<Worker>> {
    //     Ok(Arc::clone(&channel.workers)
    //         .lock()
    //         .unwrap()
    //         .iter()
    //         .map(|(_, v)| v.to_worker())
    //         .collect::<Vec<Worker>>())
    //     // .map(|(k, v)| (k.clone(), v.to_worker()))
    //     // .collect::<HashMap<String, Worker>>()[&id].clone())
    // }
    //#[graphql(description = "Add tasks")]
    async fn create_tasks(
        channel: String,
        tasks: Vec<TaskInput>,
        task_channel: &TaskChannel,
    ) -> FieldResult<Vec<Task>> {
        let mut new_tasks = vec![];
        log::info!("GraphQL: {:?}", tasks);
        for task in tasks.iter() {
            let task = Task::from((*task).clone());
            task_channel.push_and_notify(&channel[..], &task).await;
            new_tasks.push(task);
        }
        Ok(new_tasks)
    }
}
pub type Schema = RootNode<'static, Query, Mutation, EmptySubscription<TaskChannel>>;
pub fn create_schema() -> Schema {
    Schema::new(Query, Mutation, EmptySubscription::new())
}
pub async fn graphiql_route() -> Result<HttpResponse, Error> {
    graphiql_handler("/graphql", None).await
}
pub async fn playground_route() -> Result<HttpResponse, Error> {
    playground_handler("/graphql", None).await
}
pub async fn graphql_route(
    req: actix_web::HttpRequest,
    payload: actix_web::web::Payload,
    schema: web::Data<Schema>,
    channel: web::Data<TaskChannel>,
) -> Result<HttpResponse, Error> {
    graphql_handler(&schema, &channel, req, payload).await
}
