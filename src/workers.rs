use crate::connections::ConnectionType;
use clap::ArgEnum;

use juniper::{GraphQLEnum, GraphQLObject};

#[derive(Debug, Clone)]
pub struct MessageQueueWorker {
    id: String,
    name: String,
    url: String,
    channels: Vec<String>,
}
impl MessageQueueWorker {
    pub fn new(w: Worker) -> MessageQueueWorker {
        MessageQueueWorker {
            id: w.id,
            name: w.name,
            url: w.url,
            channels: w.channels,
        }
    }
    pub fn new_t(w: Worker) -> Box<dyn TWorker> {
        Box::new(MessageQueueWorker::new(w))
    }
}
impl TWorker for MessageQueueWorker {
    fn get_type(&self) -> WorkerType {
        WorkerType::MessageQueue
    }
    fn get_id(&self) -> String {
        self.id.clone()
    }
    fn to_worker(&self) -> Worker {
        Worker::new_iner(
            self.id.clone(),
            self.name.clone(),
            self.url.clone(),
            WorkerType::MessageQueue,
            self.channels.clone(),
        )
    }
    fn get_url(&self, path: &str) -> String {
        format!("{}/{}", self.url, path)
    }
    fn get_channels(&self) -> Vec<String> {
        self.channels.clone()
    }
}

#[derive(Debug, Clone)]
pub struct StreamingWorker {
    id: String,
    name: String,
    url: String,
    channels: Vec<String>,
}
impl StreamingWorker {
    pub fn new(w: Worker) -> StreamingWorker {
        StreamingWorker {
            id: w.id,
            name: w.name,
            url: w.url,
            channels: w.channels,
        }
    }
    pub fn new_t(w: Worker) -> Box<dyn TWorker> {
        Box::new(StreamingWorker::new(w))
    }
}
impl TWorker for StreamingWorker {
    fn get_type(&self) -> WorkerType {
        WorkerType::Streaming
    }
    fn get_id(&self) -> String {
        self.id.clone()
    }
    fn to_worker(&self) -> Worker {
        Worker::new_iner(
            self.id.clone(),
            self.name.clone(),
            self.url.clone(),
            WorkerType::Streaming,
            self.channels.clone(),
        )
    }
    fn get_url(&self, path: &str) -> String {
        format!("{}/{}", self.url, path)
    }
    fn get_channels(&self) -> Vec<String> {
        self.channels.clone()
    }
}

#[derive(Debug, Clone)]
pub struct ReplicaWorker {
    url: String,
    channels: Vec<String>,
}
impl ReplicaWorker {
    pub fn new(url: String, channels: Vec<String>) -> ReplicaWorker {
        ReplicaWorker { url, channels }
    }
    pub fn new_t(w: Worker) -> Box<dyn TWorker> {
        Box::new(ReplicaWorker::new(w.url, w.channels))
    }
}
impl TWorker for ReplicaWorker {
    fn get_url(&self, path: &str) -> String {
        format!("{}/{}", self.url, path)
    }
    fn get_channels(&self) -> Vec<String> {
        self.channels.clone()
    }
    fn get_type(&self) -> WorkerType {
        WorkerType::Replica
    }
    fn get_id(&self) -> String {
        self.url.clone()
    }
    fn to_worker(&self) -> Worker {
        Worker::new_iner(
            self.url.clone(),
            self.url.clone(),
            self.url.clone(),
            WorkerType::Replica,
            self.channels.clone(),
        )
    }
    fn ready(&self) -> Result<bool, String> {
        Ok(true)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, GraphQLObject)]
#[graphql(description = "Worker description")]
pub struct Worker {
    pub id: String,
    pub name: String,
    pub url: String,
    pub r#type: WorkerType,
    pub channels: Vec<String>,
}
impl Worker {
    pub fn new(
        id: &str,
        name: &str,
        url: &str,
        r#type: WorkerType,
        channels: Vec<String>,
    ) -> Worker {
        Worker {
            id: id.to_string(),
            name: name.to_string(),
            url: url.to_string(),
            r#type: r#type,
            channels,
        }
    }
    pub fn new_iner(
        id: String,
        name: String,
        url: String,
        r#type: WorkerType,
        channels: Vec<String>,
    ) -> Worker {
        Worker {
            id,
            name,
            url,
            r#type,
            channels,
        }
    }
}
impl TWorker for Worker {
    fn get_type(&self) -> WorkerType {
        self.r#type.clone()
    }
    fn get_id(&self) -> String {
        self.id.clone()
    }
    fn to_worker(&self) -> Worker {
        self.clone()
    }
    fn get_url(&self, path: &str) -> String {
        format!("{}/{}", self.url, path)
    }
    fn get_channels(&self) -> Vec<String> {
        self.channels.clone()
    }
}
#[derive(Debug, Clone)]
pub struct ShardWorker {
    url: String,
    channels: Vec<String>,
}
impl ShardWorker {
    pub fn new(url: String, channels: Vec<String>) -> Self {
        ShardWorker { url, channels }
    }
    pub fn new_t(w: Worker) -> Box<dyn TWorker> {
        Box::new(Self::new(w.url, w.channels))
    }
}
impl TWorker for ShardWorker {
    fn get_type(&self) -> WorkerType {
        WorkerType::Shard
    }
    fn get_channels(&self) -> Vec<String> {
        self.channels.clone()
    }
    fn get_id(&self) -> String {
        self.url.clone()
    }
    fn ready(&self) -> Result<bool, String> {
        Ok(false)
    }
    fn to_worker(&self) -> Worker {
        Worker::new_iner(
            self.url.clone(),
            self.url.clone(),
            self.url.clone(),
            WorkerType::Shard,
            self.channels.clone(),
        )
    }
    fn get_url(&self, path: &str) -> String {
        format!("{}/{}", self.url, path)
    }
}

#[derive(GraphQLEnum, ArgEnum, Debug, PartialEq, Eq, Hash, Clone, Copy, Serialize, Deserialize)]
pub enum WorkerType {
    Streaming,
    MessageQueue,
    Replica,
    Shard,
}

pub trait TWorker: Send + Sync + std::fmt::Debug {
    fn get_id(&self) -> String;
    fn to_worker(&self) -> Worker;
    fn get_connector_type(&self) -> ConnectionType {
        ConnectionType::Rest
    }
    fn get_url(&self, path: &str) -> String;
    fn get_type(&self) -> WorkerType;
    fn ready(&self) -> Result<bool, String> {
        let url = self.get_url("ready/");
        let t = std::thread::spawn(move || {
            let client = reqwest::blocking::Client::new();
            let mut ready = false;
            loop {
                match client.get(&url).send() {
                    Ok(res) => {
                        ready = match res.json() {
                            Ok(j) => j,
                            _ => {
                                log::error!("Failed to parse json");
                                false
                            }
                        };
                        break;
                    }
                    Err(_) => {
                        panic!("Connection failed")
                        // if e.is_connect() {
                        //     thread::sleep(time::Duration::from_millis(100));
                        // } else {
                        //     break;
                        // }
                    }
                }
            }
            ready
        });
        match t.join() {
            Ok(r) => Ok(r),
            Err(r) => Err(format!("{:?}", r)),
        }
    }
    fn get_channels(&self) -> Vec<String>;
}
