use crate::task::{Task, TaskState};
use chrono::{DateTime, Utc};
use std::collections::HashMap;
use std::fs;
use std::fs::{File, OpenOptions};
use std::io::prelude::*;
use std::io::{BufRead, BufReader};
use std::path::Path;

pub trait Storage: Sync + Send + std::fmt::Debug {
    fn add(&mut self, channel: &str, task: Task) -> Option<Task>;
    fn get_all(&self, channel: Option<&str>) -> Vec<Task>;
    fn get(&self, channel: &str, id: &str) -> Option<Task>;
    fn get_by(&self, state: TaskState, channel: Option<&str>) -> Option<Task> {
        let tasks = self.get_all(channel);
        println!("Get By state {:?}", tasks);
        for task in tasks {
            if task.state == state {
                return Some(task);
            }
        }
        None
    }
    fn get_from(&self, from: DateTime<Utc>, channel: Option<&str>) -> Option<Task> {
        let tasks = self.get_all(channel);
        println!("Get By state {:?}", tasks);
        for task in tasks {
            if task.created_at >= from {
                return Some(task);
            }
        }
        None
    }
    fn clone_box(&self) -> Box<dyn Storage>;
}

#[derive(Debug, Clone)]
pub struct BinFileStorage {
    name: String,
}
impl BinFileStorage {
    pub fn clean(&self) {
        match fs::remove_file(&self.name) {
            _ => (),
        };
    }
}
impl Storage for BinFileStorage {
    fn add(&mut self, channel: &str, task: Task) -> Option<Task> {
        let mut encoded: Vec<u8> = bincode::serialize(&task).unwrap();
        encoded.extend("\n".as_bytes());
        let path = Path::new(&self.name);
        let mut file = match OpenOptions::new().create(true).append(true).open(&path) {
            Ok(f) => f,
            _ => return None,
        };
        file.write_all(&encoded).unwrap();
        None
    }
    fn get_all(&self, channel: Option<&str>) -> Vec<Task> {
        let path = Path::new(&self.name); // !: Fix to iterate through all folders
        let file = match File::open(&path) {
            Ok(f) => f,
            _ => return vec![],
        };
        let reader = BufReader::new(file);
        let mut d: Vec<Task> = vec![];
        for line in reader.lines() {
            let line = line.unwrap();
            d.push(bincode::deserialize(&line.as_bytes()).unwrap())
        }
        d
    }
    fn get(&self, channel: &str, id: &str) -> Option<Task> {
        let all = self.get_all(Some(channel));
        for task in all {
            if task.id == *id {
                return Some(task);
            }
        }
        None
    }
    fn clone_box(&self) -> Box<dyn Storage> {
        Box::new(self.clone())
    }
}

#[derive(Debug, Clone)]
pub struct InMemoryStorage {
    map: HashMap<String, HashMap<String, Task>>,
}
impl InMemoryStorage {
    pub fn new() -> InMemoryStorage {
        InMemoryStorage {
            map: HashMap::new(),
        }
    }
}
impl Storage for InMemoryStorage {
    fn add(&mut self, channel: &str, task: Task) -> Option<Task> {
        if self.map.contains_key(channel) {
            let t = self.map.get_mut(channel).unwrap();
            let r = t.insert(task.id.clone(), task.clone());
            // log::debug!("[Storage] True: {:?}", t);
            r
        } else {
            self.map.insert(channel.to_owned(), HashMap::new());
            let t = self.map.get_mut(channel).unwrap();
            let r = t.insert(task.id.clone(), task.clone());
            // log::debug!("[Storage] False: {:?}", t);
            r
        }
    }
    fn get_all(&self, channel: Option<&str>) -> Vec<Task> {
        // log::debug!("[Storage] {:?} Map: {:?}", channel, self.map);
        // println!("[Storage] {:?} Map: {:?}", channel, self.map);
        self.map
            .iter()
            .filter(|(k, _)| match channel {
                Some(c) => c == &k[..],
                None => true,
            })
            .fold(vec![], |mut r, (_, v)| {
                r.append(&mut v.values().cloned().collect::<Vec<Task>>());
                r
            })
    }
    fn get(&self, channel: &str, id: &str) -> Option<Task> {
        match self.map.get(channel) {
            Some(x) => match x.get(id) {
                Some(x) => Some(x.clone()),
                None => None,
            },
            None => None,
        }
    }
    fn clone_box(&self) -> Box<dyn Storage> {
        Box::new(self.clone())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn bin_file_multiple_tasks() {
        let mut test = BinFileStorage {
            name: "db.txt".to_string(),
        };
        test.clean();
        let data = test.get_all(None);
        assert_eq!(Vec::<Task>::new(), data);
        let task1 = Task::new("id", "name", TaskState::NotStarted, HashMap::new());
        test.add("1", task1.clone());
        let task2 = Task::new("id2", "name2", TaskState::Finished, HashMap::new());
        test.add("1", task2.clone());
        let data = test.get_all(None);
        test.clean();
        assert_eq!(data, [task1, task2]);
    }

    #[test]
    fn bin_file() {
        let mut test = BinFileStorage {
            name: "db1.txt".to_string(),
        };
        test.clean();
        let task = Task::new("id", "name", TaskState::NotStarted, HashMap::new());
        test.add("1", task.clone());
        let data = test.get("1", "id");
        test.clean();
        assert_eq!(data, Some(task));
    }
}
