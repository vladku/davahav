use crate::task::Task;
use crate::workers::{TWorker, Worker};
use juniper::GraphQLEnum;

#[derive(GraphQLEnum, Debug, PartialEq, Eq, Hash, Clone, Copy, Serialize, Deserialize)]
pub enum ConnectionType {
    Rest,
    GraphQL,
}

pub async fn get_shard_tasks(
    r#type: ConnectionType,
    url: impl Into<String>,
) -> Result<Vec<Task>, String> {
    match r#type {
        ConnectionType::Rest => match RestConnection::get_shard_tasks(url.into()).await {
            Ok(tasks) => Ok(tasks),
            Err(e) => Err(format!("{}", e)),
        },
        _ => Err("Connection type not supported".to_string()),
    }
}

pub async fn on_notify(
    r#type: ConnectionType,
    channel: impl Into<String>,
    task: &Task,
    worker: &Worker,
) -> Result<(), String> {
    match r#type {
        ConnectionType::Rest => match RestConnection::on_notify(channel.into(), task, worker).await
        {
            Ok(r) => match r.error_for_status() {
                Ok(_) => Ok(()),
                Err(e) => Err(format!("{}", e)),
            },
            Err(e) => Err(format!("{}", e)),
        },
        _ => Err("Connection type not supported".to_string()),
    }
}

pub struct RestConnection {}
impl RestConnection {
    pub async fn get_shard_tasks(url: String) -> Result<Vec<Task>, reqwest::Error> {
        let client = reqwest::Client::new();
        client.get(url).send().await?.json::<Vec<Task>>().await
    }

    pub async fn on_notify(
        channel: String,
        task: &Task,
        worker: &Worker,
    ) -> Result<reqwest::Response, reqwest::Error> {
        let url = worker.get_url(&format!("{}/task/", channel)[..]);
        let client = reqwest::Client::new();
        client.post(url).json(&task).send().await
    }
}
