use prost::Message as Message;

// Include the `items` module, which is generated from davahav.proto.
pub mod items {
    tonic::include_proto!("davahav.items");
}

pub fn create_worker() -> items::Worker {
    let mut shirt = items::Worker::default();
    shirt.set_type(items::worker::Type::Replica);
    shirt
}

#[cfg(test)]
mod tests {
    use crate::models::*;

    #[test]
    fn test_create_worker() {
        let worker = create_worker();
        println!("{:?}", worker);
        assert_eq!(worker.r#type, 3);
    }
}
