use chrono::{DateTime, FixedOffset, Utc};
use juniper::{graphql_object, GraphQLEnum, GraphQLObject};
use std::collections::HashMap;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize, GraphQLEnum)]
pub enum TaskState {
    NotStarted,
    Reserved,
    InProgress,
    Finished,
    Error,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct TaskHistory {
    pub at: DateTime<Utc>,
    pub state: TaskState,
    pub by: String, // TODO: Think about adding data changes to history
}
impl TaskHistory {
    pub fn new(at: DateTime<Utc>, state: TaskState, by: String) -> Self {
        TaskHistory { at, state, by }
    }
}
#[graphql_object]
impl TaskHistory {
    pub fn at(&self, tz: Option<i32>) -> String {
        format_date_time(self.at, tz)
    }
    pub fn state(&self) -> TaskState {
        self.state
    }
    pub fn by(&self) -> String {
        self.by.clone()
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum Val {
    Int(i32),
    Str(String),
    None,
}
#[graphql_object]
impl Val {
    fn int(&self) -> Option<i32> {
        match *self {
            Val::Int(i) => Some(i),
            _ => None,
        }
    }
    fn str(&self) -> Option<String> {
        match self.clone() {
            Val::Str(i) => Some(i),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Task {
    pub id: String,
    pub name: String,
    pub state: TaskState,
    pub data: HashMap<String, Val>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
    pub history: Vec<TaskHistory>,
    pub next: Option<Box<Task>>,
}
impl Task {
    pub fn new(id: &str, name: &str, state: TaskState, data: HashMap<String, Val>) -> Task {
        let now = Utc::now();
        Task {
            id: id.to_string(),
            name: name.to_string(),
            state,
            data,
            created_at: now,
            updated_at: now,
            history: vec![],
            next: None,
        }
    }
}

#[graphql_object]
#[graphql(description = "Task info")]
impl Task {
    pub fn id(&self) -> String {
        self.id.clone()
    }
    pub fn name(&self) -> String {
        self.name.clone()
    }
    pub fn state(&self) -> TaskState {
        self.state
    }
    pub fn data(&self, keys: Option<Vec<String>>) -> Vec<MapEntry> {
        self.data
            .iter()
            .filter(|(key, _)| match keys.clone() {
                Some(keys) => keys.contains(key),
                None => true,
            })
            .map(|(key, value)| MapEntry {
                key: key.clone(),
                value: value.clone(),
            })
            .collect()
    }
    pub fn data_key(&self, key: String) -> Option<Val> {
        if self.data.contains_key(&key) {
            Some(self.data[&key].clone())
        } else {
            None
        }
    }
    pub fn created_at(&self, tz: Option<i32>) -> String {
        format_date_time(self.created_at, tz)
    }
    pub fn updated_at(&self, tz: Option<i32>) -> String {
        format_date_time(self.updated_at, tz)
    }
    pub fn history(&self) -> Vec<TaskHistory> {
        self.history.clone()
    }
    pub fn next(&self) -> Option<Task> {
        match self.next.clone() {
            Some(next) => Some(*next),
            _ => None,
        }
    }
}

fn format_date_time(dt: DateTime<Utc>, tz: Option<i32>) -> String {
    match tz {
        Some(tz) => dt.with_timezone(&FixedOffset::east(tz * 3600)).to_string(),
        None => dt.to_string(),
    }
}

#[derive(GraphQLObject)]
pub struct MapEntry {
    key: String,
    value: Val,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize, GraphQLEnum)]
pub enum NodeTypeGQL {
    Task,
    Condition,
    Parallel,
    End,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Condition {
    Equel(TaskState),
    EquelData(String, String),
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum NodeType {
    Task(String, Task),
    Parallel(Vec<(String, Task)>),
    Condition(Condition, Box<NodeType>, Box<NodeType>),
    End,
}

impl From<NodeType> for NodeTypeGQL {
    fn from(node_type: NodeType) -> Self {
        match node_type {
            NodeType::Task(_, _) => NodeTypeGQL::Task,
            NodeType::Condition(_, _, _) => NodeTypeGQL::Condition,
            NodeType::Parallel(_) => NodeTypeGQL::Parallel,
            NodeType::End => NodeTypeGQL::End,
        }
    }
}
