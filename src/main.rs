#[macro_use]
extern crate serde_derive;

use std::collections::HashMap;
use std::sync::Arc;

extern crate tera;
use actix_web::{delete, get, post, put, web, App, HttpResponse, HttpServer, Responder};
use clap::Parser;
use tera::{Context, Tera};

// pub mod models;
pub mod connections;
pub mod graphql;
pub mod storages;
pub mod task;
pub mod task_channel;
pub mod workers;
use crate::graphql::*;
use crate::task::*;
use crate::task_channel::*;
use crate::workers::*;

#[post("/{channel}")]
async fn push(
    path: web::Path<(String,)>,
    task: web::Json<Task>,
    task_channel: web::Data<TaskChannel>,
) -> impl Responder {
    task_channel.push_and_notify(&path.0[..], &task.0).await;
    HttpResponse::Ok()
}

#[get("/{channel}/{id}")]
async fn get(
    path: web::Path<(String, String)>,
    task_channel: web::Data<TaskChannel>,
) -> Option<web::Json<Task>> {
    match task_channel
        .storage
        .lock()
        .unwrap()
        .get(&path.0[..], &path.1)
    {
        Some(t) => Option::Some(web::Json(t.clone())),
        None => Option::None,
    }
}

#[get("/")]
async fn get_all(task_channel: web::Data<TaskChannel>) -> Option<web::Json<Vec<Task>>> {
    Some(web::Json(task_channel.get_all(None).await))
}

#[get("/{channel}")]
async fn get_all_in_channel(
    path: web::Path<(String,)>,
    task_channel: web::Data<TaskChannel>,
) -> Option<web::Json<Vec<Task>>> {
    Some(web::Json(task_channel.get_all(Some(&path.0[..])).await))
}

#[derive(Debug, Serialize, Deserialize, juniper::GraphQLInputObject)]
struct UpdateTask {
    worker_id: String,
    task: TaskInput,
    channel: String, // ? Maybe it is better to add it to url path
}

#[put("/")]
async fn update(
    update_task: web::Json<UpdateTask>,
    task_channel: web::Data<TaskChannel>,
) -> String {
    let task = update_task.0.task;
    let state = match task.state {
        Some(s) => s,
        None => TaskState::NotStarted,
    };
    let task = Task::new(&task.id, &task.name, state, to_map(&task.data));
    task_channel.update_task(&update_task.0.channel[..], task, update_task.0.worker_id);
    "Ok".to_string()
}

#[get("/")]
async fn get_all_workers(
    channel: web::Data<TaskChannel>,
) -> Option<web::Json<HashMap<String, Worker>>> {
    let workers = Arc::clone(&channel.workers);
    let workers = workers.lock().unwrap();
    log::info!("GET {:?}", workers);
    Some(web::Json(
        workers
            .iter()
            .map(|(k, v)| (k.clone(), v.to_worker()))
            .collect::<HashMap<String, Worker>>(),
    ))
}

#[post("/")]
async fn register(worker: web::Json<Worker>, channel: web::Data<TaskChannel>) -> impl Responder {
    log::info!("register {:?}", worker);
    let worker_types = Arc::clone(&channel.worker_types);
    let worker_types = worker_types.lock().unwrap();
    let f = worker_types.get(&worker.r#type).unwrap();
    channel.add_worker(f(worker.0.clone())).await;
    HttpResponse::Ok()
}
#[delete("/{id}")]
async fn unregister(path: web::Path<(String,)>, channel: web::Data<TaskChannel>) -> impl Responder {
    log::info!("unregister {:?}", path.0);
    channel.remove_worker(path.0.clone());
    HttpResponse::Ok()
}

#[put("/process/")]
async fn process(
    request: web::Json<ProcessTaskRequest>,
    task_channel: web::Data<TaskChannel>,
) -> impl Responder {
    task_channel
        .process(request.0.worker_id, &request.0.channel[..], request.0.by)
        .await;
    HttpResponse::Ok()
}
#[derive(Debug, Serialize, Deserialize)]
struct ProcessTaskRequest {
    worker_id: String,
    channel: String,
    by: ProcessBy,
}

#[get("workers")]
async fn monitor(channel: web::Data<TaskChannel>, tera: web::Data<Tera>) -> impl Responder {
    let w = Arc::clone(&channel.workers);
    let w: Vec<Worker> = w
        .lock()
        .unwrap()
        .iter()
        .map(|(_, x)| x.to_worker().clone())
        .collect();
    let mut context = Context::new();
    context.insert("page", "workers");
    context.insert("workers", &w);
    let rendered = tera.render("workers.html", &context).unwrap();
    HttpResponse::Ok().body(rendered)
}

#[get("tasks")]
async fn monitor_tasks(channel: web::Data<TaskChannel>, tera: web::Data<Tera>) -> impl Responder {
    let mut context = Context::new();
    context.insert("page", "tasks");
    context.insert("tasks", &channel.get_all(None).await);
    let rendered = tera.render("tasks.html", &context).unwrap();
    HttpResponse::Ok().body(rendered)
}

/// PubSub and Queue manager tool
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// URL of master node
    #[clap(short, long)]
    master_url: Option<String>,

    /// Worker type
    #[clap(short='t', long, default_value_t = WorkerType::Replica, arg_enum)]
    worker_type: WorkerType,

    /// Port
    #[clap(short = 'p', long, default_value_t = 9233)]
    port: u16,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("debug"));
    let args = Args::parse();
    let channel = match args.master_url {
        None => TaskChannel::new(),
        Some(master_url) => TaskChannel::new_node(
            args.worker_type,
            master_url,
            format!("http://{}:{}", "127.0.0.1", args.port),
        ),
    };
    let tera = Tera::new("templates/**/*").unwrap();
    log::info!("starting HTTP server on port 9233");
    log::info!("GraphiQL playground: http://localhost:9233/graphiql");

    // let rpc = RPCServer { };
    // let rpc = rpc::davahav_server::DavahavServer::new(rpc);
    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(tera.clone()))
            .app_data(web::Data::new(channel.clone()))
            .app_data(web::Data::new(create_schema()))
            .service(
                web::resource("/graphql")
                    .route(web::post().to(graphql_route))
                    .route(web::get().to(graphql_route)),
            )
            .service(web::resource("/playground").route(web::get().to(playground_route)))
            .service(web::resource("/graphiql").route(web::get().to(graphiql_route)))
            .service(
                web::scope("/worker")
                    .service(register)
                    .service(unregister)
                    .service(get_all_workers)
                    .service(process),
            )
            .service(
                web::scope("/task")
                    .service(get)
                    .service(push)
                    .service(get_all)
                    .service(get_all_in_channel)
                    .service(update),
            )
            .service(monitor)
            .service(monitor_tasks)
            .service(actix_files::Files::new("/static", "./static").show_files_listing())
    })
    .bind(("127.0.0.1", args.port))?
    .workers(2)
    .run()
    .await
}

// pub mod rpc {
//     tonic::include_proto!("davahav.items");
// }
// struct RPCServer {}

// impl actix_web::dev::HttpServiceFactory for RPCServer {
//     fn register(self, config: &mut actix_web::dev::AppService) {
//         config.register_service(rdef: ResourceDef, guards: Option<Vec<Box<dyn Guard>>>, factory: F, nested: Option<Rc<ResourceMap>>)
//     }
// }

// #[tonic::async_trait]
// impl rpc::davahav_server::Davahav for RPCServer {
//     async fn get_worker(
//         &self,
//         request: tonic::Request<()>,
//     ) -> Result<tonic::Response<rpc::Worker>, tonic::Status> {
//         Ok(tonic::Response::new(rpc::Worker::default()))
//     }
// }

#[cfg(test)]
mod unit {
    use super::*;
    use crate::storages::Storage;
    use nanoid::nanoid;
    use pretty_assertions::assert_eq;
    use std::sync::Mutex;

    macro_rules! in_arc {
        ($arc:expr, $func:expr) => {{
            let a = Arc::clone($arc);
            let a = &*a.lock().unwrap();
            $func(a);
        }};
    }
    macro_rules! in_arc_mut {
        ($arc:expr, $func:expr) => {{
            let a = Arc::clone($arc);
            let a = &mut *a.lock().unwrap();
            $func(a);
        }};
    }

    #[derive(Debug, Clone)]
    pub struct TestMessageQueueWorker {
        id: String,
        ready: bool,
        channel: TaskChannel,
        processed: Arc<Mutex<Vec<Task>>>,
    }
    impl TestMessageQueueWorker {
        pub fn new(channel: TaskChannel) -> TestMessageQueueWorker {
            TestMessageQueueWorker {
                ready: true,
                id: nanoid!(10),
                channel: channel,
                processed: Arc::new(Mutex::new(vec![])),
            }
        }
    }
    impl TWorker for TestMessageQueueWorker {
        fn get_channels(&self) -> Vec<String> {
            vec!["1".to_owned()]
        }
        fn get_type(&self) -> WorkerType {
            WorkerType::MessageQueue
        }
        fn get_id(&self) -> String {
            self.id.clone()
        }
        fn get_url(&self, path: &str) -> String {
            format!("/{}", path)
        }
        fn ready(&self) -> Result<bool, String> {
            Ok(self.ready)
        }
        fn to_worker(&self) -> Worker {
            Worker::new(
                &self.id[..],
                "Test",
                "url",
                WorkerType::MessageQueue,
                vec!["1".to_owned()],
            )
        }
    }

    #[derive(Debug, Clone)]
    pub struct TestStreamingWorker {
        id: String,
        ready: bool,
        task_id: Option<String>,
        channel: TaskChannel,
        processed: Arc<Mutex<Vec<Task>>>,
    }
    impl TestStreamingWorker {
        pub fn new(channel: TaskChannel, task_id: Option<String>) -> TestStreamingWorker {
            TestStreamingWorker {
                ready: true,
                id: nanoid!(10),
                task_id: task_id,
                channel: channel,
                processed: Arc::new(Mutex::new(vec![])),
            }
        }
    }
    impl TWorker for TestStreamingWorker {
        fn get_channels(&self) -> Vec<String> {
            vec!["1".to_owned()]
        }
        fn get_type(&self) -> WorkerType {
            WorkerType::Streaming
        }
        fn get_id(&self) -> String {
            self.id.clone()
        }
        fn get_url(&self, path: &str) -> String {
            format!("/{}", path)
        }
        fn ready(&self) -> Result<bool, String> {
            Ok(self.ready)
        }
        fn to_worker(&self) -> Worker {
            Worker {
                id: self.id.clone(),
                name: "Test".to_string(),
                url: "url".to_string(),
                r#type: WorkerType::Streaming,
                channels: vec!["*".to_owned()],
            }
        }
    }
    fn test_on_notify(task_channel: &TaskChannel, channel: String, task: &Task, worker: &Worker) {
        match worker.r#type {
            WorkerType::Replica => (),
            _ => {
                let mut task = task.clone();
                task.state = TaskState::InProgress;
                task_channel.update_task(&channel[..], task.clone(), worker.get_id());
                task.state = TaskState::Finished;
                task_channel.update_task(&channel[..], task, worker.get_id());
            }
        };
    }

    impl TaskChannel {
        pub fn wait_workers(&self) {
            let ths = Arc::clone(&self.ths);
            let mut ths = ths.lock().unwrap();
            while ths.len() > 0 {
                println!("{:?}", ths.pop().unwrap().join());
            }
        }
    }

    #[tokio::test]
    async fn task_not_started_without_workers() {
        let channel = TaskChannel::new();
        channel
            .push_and_notify(
                "1",
                &Task::new("id", "name", TaskState::NotStarted, HashMap::new()),
            )
            .await;
        channel.wait_workers();

        in_arc_mut!(&channel.storage, |storage: &mut Box<dyn Storage>| {
            let storage = storage.get_all(None);
            let in_channel = storage.iter().next().unwrap();
            assert_eq!(TaskState::NotStarted, in_channel.state);
        });
    }

    #[tokio::test]
    async fn register_worker_without_task_in_queue() {
        let channel = TaskChannel::new();
        let mut workers = vec![];
        let worker = TestMessageQueueWorker::new(channel.clone());
        workers.push(worker.clone());
        channel.add_worker(Box::new(worker.clone())).await;
        channel.wait_workers();

        in_arc!(&channel.workers, |x: &HashMap<String, Box<dyn TWorker>>| {
            assert_eq!(1, x.len());
        });
        in_arc_mut!(&channel.storage, |x: &mut Box<dyn Storage>| {
            assert_eq!(0, x.get_all(None).len());
        });
    }

    #[tokio::test]
    async fn one_task_at_time() {
        let channel = TaskChannel::new();
        let mut worker = TestMessageQueueWorker::new(channel.clone());
        worker.ready = false;
        channel.add_worker(Box::new(worker.clone())).await;
        channel
            .push_and_notify(
                "1",
                &Task::new("1", "name", TaskState::NotStarted, HashMap::new()),
            )
            .await;
        channel
            .push_and_notify(
                "1",
                &Task::new("2", "name", TaskState::NotStarted, HashMap::new()),
            )
            .await;
        channel.wait_workers();

        in_arc_mut!(&channel.storage, |x: &mut Box<dyn Storage>| {
            let x = x.get_all(None);
            assert_eq!(2, x.len());
            x.iter()
                .for_each(|x| assert_eq!(x.state, TaskState::NotStarted));
        });

        in_arc!(&worker.processed, |x: &Vec<Task>| {
            assert_eq!(0, x.len());
        });
    }
}

#[cfg(test)]
mod api {
    use super::unit::TestMessageQueueWorker;
    use super::*;
    use actix_web::{test, web, App};

    #[actix_web::test]
    async fn not_found_404() {
        let channel = TaskChannel::new();
        channel
            .add_worker(Box::new(TestMessageQueueWorker::new(channel.clone())))
            .await;
        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(channel.clone()))
                .service(web::scope("/worker").service(get_all_workers)),
        )
        .await;
        let req = test::TestRequest::get().uri("/worker/").to_request();
        let resp = test::call_service(&app, req).await;
        println!("oo -+ o {:?}", resp);
        println!("oo -+ o {:?}", resp.into_body());
        // assert!(false);
        // assert!(resp.status().is_client_error());
    }

    #[actix_web::test]
    async fn create_get_tasks() {
        let channel = TaskChannel::new();
        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(channel.clone()))
                .service(web::scope("/task").service(get_all).service(push)),
        )
        .await;
        for id in '1'..'9' {
            let req = test::TestRequest::post()
                .set_json(Task::new(
                    &id.to_string()[..],
                    "test",
                    TaskState::NotStarted,
                    HashMap::new(),
                ))
                .uri("/task/1")
                .to_request();
            test::call_service(&app, req).await;
        }
        let req = test::TestRequest::get().uri("/task/").to_request();
        let resp = test::call_service(&app, req).await;
        let rs: Vec<Task> = test::read_body_json(resp).await;
        assert_eq!(8, rs.len());
    }

    #[actix_web::test]
    async fn task_processed_by_registered_worker() {
        let channel = TaskChannel::new();
        let w = TestMessageQueueWorker::new(channel.clone());
        channel.add_worker(Box::new(w.clone())).await;
        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(channel.clone()))
                .service(web::scope("/task").service(get_all).service(push))
                .service(web::scope("/worker").service(get_all_workers)),
        )
        .await;

        let req = test::TestRequest::get().uri("/worker/").to_request();
        let resp = test::call_service(&app, req).await;
        let rt: HashMap<String, Worker> = test::read_body_json(resp).await;
        assert_eq!(1, rt.len());
        let worker_id = rt.keys().next().unwrap();
        assert_eq!(&w.get_id(), worker_id);

        let req = test::TestRequest::post()
            .set_json(Task::new(
                "1",
                "test",
                TaskState::NotStarted,
                HashMap::new(),
            ))
            .uri("/task/")
            .to_request();
        test::call_service(&app, req).await;

        channel.wait_workers();

        let req = test::TestRequest::get().uri("/task/").to_request();
        let resp = test::call_service(&app, req).await;
        let rt: Vec<Task> = test::read_body_json(resp).await;
        // match &rt[0].processed_by {
        //     Some(x) => assert!(x.contains_key(worker_id)),
        //     None => assert!(false, "Processed by not found")
        // }
    }

    //     #[test]
    //     fn register_worker() {
    //         let channel = TaskChannel::new();
    //         let worker_types = Arc::clone(&channel.worker_types);
    //         worker_types
    //             .lock()
    //             .unwrap()
    //             .insert(WorkerType::MessageQueue, TestMessageQueueWorker::new_t);
    //         let client = Client::new(test_rocket(channel)).expect("valid rocket instance");
    //         let get_workers = || {
    //             let mut response = client.get("/worker").dispatch();
    //             assert_eq!(response.status(), Status::Ok);
    //             let rs = response.body_string().unwrap();
    //             let rt: HashMap<String, Worker> = serde_json::from_str(&rs[..]).unwrap();
    //             rt
    //         };
    //         assert_eq!(0, get_workers().len());
    //         let response = client
    //             .post("/worker")
    //             .header(ContentType::JSON)
    //             .body(
    //                 r#"{
    //                     "id": "1",
    //                     "name": "test",
    //                     "ready": true,
    //                     "type": "MessageQueue",
    //                     "url": "url",
    //                     "can_process": ["*"]
    //                 }"#,
    //             )
    //             .dispatch();
    //         assert_eq!(response.status(), Status::Ok);
    //         assert_eq!(1, get_workers().len());
    //     }

    //     #[test]
    //     fn not_send_task_to_worker_that_cannot_process_task() {
    //         let channel = TaskChannel::new();
    //         let w = TestMessageQueueWorker::new(channel.clone(), vec!["some task name".to_string()]);
    //         channel.add_observer(Box::new(w.clone()), None);
    //         let client = Client::new(test_rocket(channel)).expect("valid rocket instance");

    //         let mut response = client
    //             .post("/task")
    //             .header(ContentType::JSON)
    //             .body(
    //                 r#"{
    //                     "id": "1",
    //                     "name": "test",
    //                     "state": "NotStarted",
    //                     "data": {},
    //                     "processed_by": {}
    //                 }"#,
    //             )
    //             .dispatch();
    //         assert_eq!(response.status(), Status::Ok);
    //         assert!(response.body().is_none());

    //         let mut response = client.get("/worker").dispatch();
    //         assert_eq!(response.status(), Status::Ok);
    //         let rs = response.body_string().unwrap();
    //         let rt: HashMap<String, Worker> = serde_json::from_str(&rs[..]).unwrap();
    //         assert_eq!(1, rt.len());

    //         let mut response = client.get("/task").dispatch();
    //         assert_eq!(response.status(), Status::Ok);
    //         let rs = response.body_string().unwrap();
    //         let rt: Vec<Task> = serde_json::from_str(&rs[..]).unwrap();
    //         let t = Task::new("1", "test", TaskState::NotStarted, HashMap::new());
    //         assert_eq!(rt[0], t);
    //     }

    //     #[test]
    //     fn register_new_node() {
    //         let channel = TaskChannel::new();
    //         let client = Client::new(test_rocket(channel)).expect("valid rocket instance");

    //         let response = client
    //             .post("/worker")
    //             .header(ContentType::JSON)
    //             .body(
    //                 r#"{
    //                     "id": "test_node_url",
    //                     "name": "test_node_url",
    //                     "ready": true,
    //                     "type": "Replica",
    //                     "url": "test_node_url",
    //                     "can_process": ["*"]
    //                 }"#,
    //             )
    //             .dispatch();
    //         assert_eq!(response.status(), Status::Ok);

    //         let mut response = client.get("/worker").dispatch();
    //         assert_eq!(response.status(), Status::Ok);
    //         let rs = response.body_string().unwrap();
    //         let rt: HashMap<String, Worker> = serde_json::from_str(&rs[..]).unwrap();
    //         assert_eq!("test_node_url".to_string(), rt["test_node_url"].url);
    //     }
}
